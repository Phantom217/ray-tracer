use std::{ops::Range, sync::Arc};

use super::ray::Ray;

mod hitrecord;
mod sphere;

pub use hitrecord::HitRecord;
pub use sphere::{MovingSphere, Sphere};

pub trait Hittable: Send + Sync {
    #[must_use]
    fn hit(&self, ray: &Ray, t: Range<f64>) -> Option<HitRecord>;
}

pub type World = Vec<Arc<dyn Hittable>>;
pub type WorldSlice = [Arc<dyn Hittable>];

impl Hittable for WorldSlice {
    fn hit(&self, ray: &Ray, t: Range<f64>) -> Option<HitRecord> {
        let mut hr = None;
        let mut closest_so_far = t.end;

        for object in self {
            if let Some(tmp_hr) = object.hit(ray, t.start..closest_so_far) {
                closest_so_far = tmp_hr.t();
                hr = Some(tmp_hr);
            }
        }

        hr
    }
}

impl Hittable for World {
    fn hit(&self, ray: &Ray, t: Range<f64>) -> Option<HitRecord> {
        self.as_slice().hit(ray, t)
    }
}
