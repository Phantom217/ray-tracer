use std::ops::Range;
use std::sync::Arc;

use super::{HitRecord, Hittable};
use crate::{material::Material, ray::Ray, vec::Point3};

/// Type representing stationary spheres.
pub struct Sphere {
    center: Point3,
    radius: f64,
    material: Arc<dyn Material>,
}

/// Type representing moving spheres.
pub struct MovingSphere {
    center: (Point3, Point3),
    radius: f64,
    material: Arc<dyn Material>,
    time: Range<f64>,
}

impl Sphere {
    pub fn new(center: Point3, radius: f64, material: Arc<dyn Material>) -> Self {
        Self {
            center,
            radius,
            material,
        }
    }
}

impl MovingSphere {
    pub fn new(
        center0: Point3,
        center1: Point3,
        radius: f64,
        material: Arc<dyn Material>,
        time: Range<f64>,
    ) -> Self {
        Self {
            center: (center0, center1),
            radius,
            material,
            time,
        }
    }

    pub fn center(&self, time: f64) -> Point3 {
        let (center0, center1) = self.center;
        let time0 = self.time.start;
        let time1 = self.time.end;
        center0 + ((time - time0) / (time1 - time0)) * (center1 - center0)
    }
}

impl Hittable for Sphere {
    fn hit(&self, ray: &Ray, time_range: Range<f64>) -> Option<HitRecord> {
        let oc = ray.origin() - self.center;
        let a = ray.direction().length().powi(2);
        let half_b = oc.dot(ray.direction());
        let c = oc.length().powi(2) - self.radius.powi(2);

        let discriminant = half_b.powi(2) - a * c;
        if discriminant < 0.0 {
            return None;
        }

        // Find the nearest root that lies in the acceptable range
        let sqrtd = discriminant.sqrt();
        let mut root = (-half_b - sqrtd) / a;
        if !time_range.contains(&root) {
            root = (-half_b + sqrtd) / a;
            if !time_range.contains(&root) {
                return None;
            }
        }

        let t = root;
        let p = ray.at(root);
        let outward_normal = (p - self.center) / self.radius;
        let material = Arc::clone(&self.material);
        Some(HitRecord::new(ray, p, outward_normal, material, t))
    }
}

impl Hittable for MovingSphere {
    fn hit(&self, ray: &Ray, time_range: Range<f64>) -> Option<HitRecord> {
        let center = self.center(ray.time());
        let oc = ray.origin() - center;
        let a = ray.direction().length().powi(2);
        let half_b = oc.dot(ray.direction());
        let c = oc.length().powi(2) - self.radius.powi(2);

        let discriminant = half_b.powi(2) - a * c;
        if discriminant < 0_f64 {
            return None;
        }

        // Find nearest root that lies in the acceptable range
        let sqrtd = discriminant.sqrt();
        let mut root = (-half_b - sqrtd) / a;
        if !time_range.contains(&root) {
            root = (-half_b + sqrtd) / a;
            if !time_range.contains(&root) {
                return None;
            }
        }

        let time = root;
        let p = ray.at(time);
        let outward_normal = (p - center) / self.radius;
        let material = Arc::clone(&self.material);

        Some(HitRecord::new(ray, p, outward_normal, material, time))
    }
}
